<?php

$a = array('hello', 'something','how are you');
echo is_array($a);
$b = "hello ";
echo is_array($b);
if(is_array($a)){
    echo "this is an array <br>";
}else{
    echo "this is not array";
}
if(is_array($b)){
    echo "this is an array";
}else{
    echo "this is not array <br>";
}



$var1 = 'Hello World';
$var2 = ' ';

$var2 =& $var1;

//debug_zval_dump(&$var1);

function refcount(&$var) {
    ob_start();
    debug_zval_dump(array(&$var));
    return preg_replace("/^.+?refcount\((\d+)\).+$/ms", '$1', substr(ob_get_clean(), 24), 1) - 4;
}
$a = 34;
refcount($a) == 0;

$b = &$a;
refcount($a) == 1;