<?php
$a = 1.8;
 $b = floatval($a);
 echo $b . "<br>";
 
function Getfloat($str) { 
  if(strstr($str, ",")) { 
    $str = str_replace(".", "", $str); // replace dots (thousand seps) with blancs 
    $str = str_replace(",", ".", $str); // replace ',' with '.' 
  } 
  
  if(preg_match("#([0-9\.]+)#", $str, $match)) { // search for number that may contain '.' 
    return floatval($match[0]); 
  } else { 
    return floatval($str); // take some last chances with floatval 
  } 
} 

echo Getfloat("$ 19.332,35-"); // will print: 19332.35

